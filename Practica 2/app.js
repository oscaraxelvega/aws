import express from "express";
import { fileURLToPath } from "url";
import json from "body-parser";
import path from "path";
import misRutas from "./router/index.js";

const puerto = 80;
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

//declarar la variable punto de inicio

const main = express();

main.set("view engine", "ejs");
main.set(express.static(__dirname + "/public"));
main.use(json.urlencoded({ extended: true }));
main.use(misRutas.router);

main.listen(puerto, () => {
	console.log("se inicio el servidor por el puerto: " + puerto);
});